
Lists VariantSets in the GDM. 

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with [List of [VariantSet Resource](#variantsetresource)] in result.data.


